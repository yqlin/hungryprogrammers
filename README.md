# Required java libraries to run web scraper mysql:

## JSOUP:

http://jsoup.org/packages/jsoup-1.8.1.jar

## MYSQL CONNECTOR:
http://central.maven.org/maven2/mysql/mysql-connector-java/5.1.23/mysql-connector-java-5.1.23.jar

## XAMPP FOR MYSQL DATABASE AND PHP APACHE:
https://www.apachefriends.org/index.html

## MYSQL WORKBENCH:
http://dev.mysql.com/downloads/workbench/
