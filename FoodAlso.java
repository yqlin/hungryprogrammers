import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.sql.*;
import java.util.EventObject;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.mysql.jdbc.Blob;


public class FoodAlso 
{
	final JFrame frame = new JFrame();
	final JScrollPane scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	
	final JPanel panel = new JPanel();
	final JPanel leftCorner = new JPanel();
	final JButton images [] = new JButton[77];
	
	final JLabel label [] = new JLabel[77];
	JFrame savedFrame = null;
	
	FoodAlso()
	{
		scroll.setPreferredSize(new Dimension(3000,2000));//new Dimension(1928,1038)
		new Grub();
		try 
		{
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frame.getContentPane().setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(1928, 1038);
		leftCorner.setSize(50, 50);
		leftCorner.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 0));
		leftCorner.setAlignmentX(SwingConstants.LEFT);
		leftCorner.add(new JLabel("Search"));
		leftCorner.add(new JTextArea(1,10));
		leftCorner.add(new JButton("GO"));
		leftCorner.add(new JLabel("FOODFLIX",SwingConstants.CENTER));
		scroll.setBounds(2, 50, 50, 50);
		panel.setPreferredSize(new Dimension(2500,2500));//100200  
		panel.setLayout(new GridLayout(8,8,10,15));
		scroll.setColumnHeaderView(leftCorner);
		drawImage();
		for(int i = 0; i < 77; i++)
		{
			images[i].addMouseListener(new MouseListener()
			{
				
				public void actionPerformed(ActionEvent e) 
				{
					
				}

				@Override
				public void mouseClicked(MouseEvent e) 
				{
					final JButton button = (JButton) e.getSource();
					button.setPreferredSize(new Dimension(100,100));
					String str = "'" + button.getIcon().toString() + "'";
					final JFrame frame2 = new JFrame("How to cook");
					frame2.setSize(500, 245);
					frame2.setLayout(new FlowLayout());
					
					JTextArea text = new JTextArea();
					text.setColumns(40);
					text.setRows(10);
					text.setLineWrap(true);
					String DB_URL = "jdbc:mysql://localhost/hungry_programmers";
					String USER = "root";
					String PASS = "";
					Connection conn = null;
					Statement stmt = null;
					String copyFirst = null;
					String copySecond = null;
					
					try
					{
						Class.forName("com.mysql.jdbc.Driver");
						//System.out.println("Connecting to a selected database...");
						conn = DriverManager.getConnection(DB_URL, USER, PASS);
						//System.out.println("Connected database successfully...");
						//System.out.println("Creating statement...");
						stmt = conn.createStatement();
						
						ResultSet rs = null;
						//String sql = "SELECT id, name, instructions FROM recipes WHERE image_url = " + str + "";
						String sql2 = "Select recipes.name, ingredients.name from recipes join ingredients_in_recipes on recipes.id = recipe_id join ingredients on ingredients.id = ingredient_id where recipe_id = recipes.id AND recipes.image_url = " + str + " AND ingredients.name IS NOT NULL " ;
						rs = stmt.executeQuery(sql2);
						//String instructions = null;
						while (rs.next()) 
						{
							String first = rs.getString("recipes.name");
							String second = rs.getString("ingredients.name");
							copyFirst = first;
							copySecond += second + "\n";	
						}
						text.setText(copySecond);
						
						rs.close();
						conn.close();
						stmt.close();
					}
					catch(Exception se)
					{
						se.printStackTrace();
					}
					frame2.getContentPane().add(new JLabel(copyFirst));
					frame2.getContentPane().add(text);
					frame2.setVisible(true);
					
				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		frame.add(Box.createHorizontalStrut(200));
		scroll.setViewportView(panel);
		frame.setContentPane(scroll);
		frame.setVisible(true);
	}
	public void drawImage()
	{
		for(int i = 0; i < 77; i++)
		{	
			label[i] = new JLabel(Grub.title[i]);
			images[i] = new JButton(label[i].getText(), new ImageIcon(Grub.location[i]));
			images[i].setVerticalTextPosition(SwingConstants.BOTTOM);
			images[i].setHorizontalTextPosition(SwingConstants.CENTER);
			images[i].setPreferredSize(new Dimension(400,400));//100100 
			panel.add(images[i]);
		}
	}
	public static void main(String[] args) 
	{
		SwingUtilities.invokeLater(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				new FoodAlso();
			}
			
		});
		
	}
}
