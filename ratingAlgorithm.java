import java.sql.*;
import java.util.*;

public class ratingAlgorithm
{
    final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //URL to the DB. 
    final static String DB_URL = "jdbc:mysql://localhost";
    //Login credientials. Ensure that yours are here.
    final static String USER = "root";
    final static String PASS = "";
    //maybe it works?

    public static int recipe_rate(double newrating, String recipename)
    {
        Connection conn = null;
        PreparedStatement prepstat = null;
        PreparedStatement recpst = null;
        PreparedStatement updater = null;

        try
        {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //easy part, getting the small stuff sorted. Get rating, update rating.
            prepstat = conn.prepareStatement("SELECT rating FROM hungry_programmers.recipes WHERE name = ?;");
            prepstat.setString(1, recipename);
            //standard operating procedure more or less
            ResultSet rs = prepstat.executeQuery();
            double oldrating = 0.0;
            while(rs.next()){
                oldrating = rs.getDouble("rating");
            }

            int ratingdif = (int)newrating - (int)oldrating; //this, however, is for the harder part. The crazy suggestion algorithm part.

            prepstat = conn.prepareStatement("UPDATE hungry_programmers.recipes SET rating = ? WHERE name = ?;");
            prepstat.setBigDecimal(1, new java.math.BigDecimal(newrating));
            prepstat.setString(2, recipename);
            prepstat.executeUpdate();   //first, set the new recipe rating.

            //easy part now done. Hard part inbound.

            prepstat = conn.prepareStatement("SELECT id FROM hungry_programmers.recipes WHERE name = ?;");
            prepstat.setString(1, recipename);
            ResultSet rs2 = prepstat.executeQuery();
            int id = 0;
            while(rs2.next()){
            	id = rs2.getInt("id");	//need this right quick, for the ingredients snafu.
            }

            //identify ingredients individually
            prepstat = conn.prepareStatement("SELECT * FROM hungry_programmers.recipes JOIN hungry_programmers.ingredients_in_recipes ON hungry_programmers.recipes.id = hungry_programmers.ingredients_in_recipes.recipe_id JOIN hungry_programmers.ingredients.id = hungry_programmers.ingredients_in_recipes.recipe_id WHERE hungry_programmers.recipes.id = ?;");
            prepstat.setInt(1, id);
            ResultSet rs3 = prepstat.executeQuery();
            //that wasn't so hard. Next part is the hard-er part.
            while(rs3.next())
            {
            	//while looking at each one, go through and update the commonuse int
            	int checker = rs3.getInt("id");
            	
            	recpst = conn.prepareStatement("SELECT * FROM hungry_programmers.ingredients WHERE id = ?;");
            	recpst.setInt(1, checker);
            	ResultSet recursive = recpst.executeQuery();
            	while(recursive.next())
            	{
            		//this is so messy...
					int oldcomval = recursive.getInt("commonuse");
					int newcomval = oldcomval + ratingdif;
					updater = conn.prepareStatement("UPDATE hungry_programmers.ingredients SET commonuse = ? WHERE id = ?;");
					updater.setInt(1, newcomval);
					updater.setInt(2, checker);
					updater.executeUpdate();
					//this should all work. Someone test it.
            	}
            	
                recpst = conn.prepareStatement("UPDATE hungry_programmers.ingredients SET newset = TRUE WHERE name = ?;");
                recpst.setString(1, recipename);
                recpst.executeUpdate();
            }

            prepstat.close();
            recpst.close();
            updater.close();
            conn.close();
            return 1;
        }
        //in the event of emergency, press all the buttons
    	catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(prepstat!=null)
                    prepstat.close();
                if(recpst!=null)
                    recpst.close();
                if(updater!=null)
                    updater.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
    	}
        //we can't just well return nothing now can we
		return -1;
	}

	public static int valupdater()	//updates the value int of recipes
	{
        Connection conn = null;
        PreparedStatement prepstat = null;
        PreparedStatement subprepst = null;
        PreparedStatement subprep2 = null;
        PreparedStatement subprep3 = null;

        try
        {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            prepstat = conn.prepareStatement("SELECT id FROM hungry_programmers.ingredients WHERE newset = TRUE;");
            ResultSet rs = prepstat.executeQuery();
            int ingredientid = 0;
            while(rs.next())
            {
            	ingredientid = rs.getInt("id");
            	subprepst = conn.prepareStatement("UPDATE hungry_programmers.ingredients SET newset = FALSE WHERE id = ?;");	//we're udating, so newset is no longer true
            	subprepst.setInt(1, ingredientid);
            	subprepst.executeUpdate();

            	subprepst = conn.prepareStatement("SELECT * FROM hungry_programmers.ingredients_in_recipes JOIN hungry_programmers.ingredients ON ingredient_id = ingredients.id JOIN hungry_programmers.recipes ON recipe_id = hungry_programmers.recipes.id WHERE ingredients.id = ?;");
            	//if I'm not mistaken that should get the recipes with this particular ingredient. If I'm wrong this will be awkward.
            	subprepst.setInt(1, ingredientid);
            	subprepst.executeQuery();
            	ResultSet subrs = subprepst.executeQuery();
            	while(subrs.next())
            	{
            		int updaterecipe = subrs.getInt("id");
            		int newvalue = 0;

            		subprep2 = conn.prepareStatement("SELECT * FROM hungry_programmers.recipes JOIN hungry_programmers.ingredients_in_recipes ON hungry_programmers.recipes.id = hungry_programmers.ingredients_in_recipes.recipe_id JOIN hungry_programmers.ingredients.id = hungry_programmers.ingredients_in_recipes.recipe_id WHERE hungry_programmers.recipes.id = ?;");
                    //need to get the individual ingredients
            		subprep2.setInt(1, updaterecipe);
            		ResultSet subrs2 = subprep2.executeQuery();
            		while(subrs2.next())
            		{
            			int thisingid = subrs2.getInt("id");
            			subprep3 = conn.prepareStatement("SELECT commonuse FROM hungry_programmers.ingredients WHERE ingredients.id = ?;");
                        //get the commonuse int of the ingredient
            			subprep3.setInt(1, thisingid);
            			ResultSet microset = subprep3.executeQuery();
            			int commonuse = 0;
            			while(microset.next())
            			{
            				commonuse = microset.getInt("commonuse");
            			}
            			newvalue = newvalue + commonuse; //add them all together
            		}

                    subprep2 = conn.prepareStatement("UPDATE hungry_programmers.recipes SET value = ? WHERE id = ?;");
                    subprep2.setInt(1, newvalue);
                    subprep2.setInt(2, updaterecipe);
                    subprep2.executeUpdate();   //and that's the new value for the recipe
                    //after this, the query to search for most likeable top down would be ("SELECT * FROM hungry_programmers.recipes ORDER BY value DESC;") if I'm not mistaken.
                    //for suggested items, ("SELECT * FROM hungry_programmers.recipes WHERE user_rating = 0 ORDER BY value DESC;"). Can't suggest what they already like...
            	}
            }

            prepstat.close();
            subprepst.close();
            subprep2.close();
            subprep3.close();
            conn.close();
            return 1;
        }
		catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(prepstat!=null)
                    prepstat.close();
                if(subprepst!=null)
                	subprepst.close();
                if(subprep2!=null)
                	subprep2.close();
                if(subprep3!=null)
                	subprep3.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
    	}


		return -1;
	}


	public static void main(String[] args)
	{
        System.out.println("Test");
        Scanner input = new Scanner(System.in);

        System.out.print("Input recipe name: ");
        String recip = input.nextLine();
        System.out.println("");
        System.out.print("Input new rating: ");
        double ratin = input.nextDouble();

        recipe_rate(ratin, recip);
        valupdater();
	}
}