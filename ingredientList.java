import java.io.IOException;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class ingredientList{
    public static void main(String args[]) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        int totalCount = 0;
        int commonUse = 0;

        //read the website
        Document doc = null;
        try {
            doc = Jsoup.connect("http://www.food.com/library/all.zsp").get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //select only the text and put in array by every space it finds
        String text = doc.body().text();
        String[] ingredientSingleWord= text.split(" ");

        //add ingredients to array list
        for (int i = 143; i < 1634; i++) {
            list.add(ingredientSingleWord[i]);
        }

        //print array list for debug
        //        for (int j = 0; j < list.size(); j++) {
        //        System.out.println(list.get(j));
        //           totalCount++;
        //        }
        //        System.out.println("Total word count:" + totalCount);
        final long startTime = System.currentTimeMillis();
        for (String s : list) {
            if (s.length() > 2)
                Importer.create_new_ingredient(s,commonUse);
        }
        final long endTime = System.currentTimeMillis();
        System.out.println("Took "+(endTime - startTime)+" ms to import all ingredients. ("+(endTime-startTime)/1000+" s)");

    }

}