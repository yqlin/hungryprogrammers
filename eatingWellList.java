import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class eatingWellList{

    public static ArrayList<String> getLinks() throws IOException {
        ArrayList<String> list = new ArrayList<>();

        //Reading HTML page from URL, loop for 199 pages at the moment
        //Loop set at 2 for testing
        for (int i = 1; i < 198; i++) {
            Document doc = null;
            try {
                doc = Jsoup.connect("http://www.eatingwell.com/recipes/browse_all_recipes?page=" + i).get();

            } catch (IOException e) {
                e.printStackTrace();
            }

            //Filter hrefs to only contain /recipes/
            Elements links = doc.getElementsByClass("view-content").select("a[href*=/recipes/]");

            //Elements links = doc.select("a[href]:contains(recipes)");

            //Loop through elements and only add the href to array list
            for (Element link : links) {
                list.add(link.attr("abs:href"));
            }
        }
        return removeDuplicates(list);
    }

    public static void main(String args[]) throws IOException {

        ArrayList<String> list = new ArrayList<>();

        //Reading HTML page from URL, loop for 199 pages at the moment
        //Loop set at 2 for testing
        for (int i = 1; i < 2; i++) {
            Document doc = null;
            try {
                doc = Jsoup.connect("http://www.eatingwell.com/recipes/browse_all_recipes?page=" + i).get();

            } catch (IOException e) {
                e.printStackTrace();
            }

            //Filter hrefs to only contain /recipes/
            Elements links = doc.getElementsByClass("view-content").select("a[href*=/recipes/]");

            //Elements links = doc.select("a[href]:contains(recipes)");

            //Loop through elements and only add the href to array list
            for (Element link : links) {
                list.add(link.attr("abs:href"));
            }

            File file = new File("eatingwell.txt");
            //Create new file
            if(!file.exists()){
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            //Here false is to rewrite the content to file
            FileWriter fw = new FileWriter(file,false);
            //BufferedWriter writer give better performance
            BufferedWriter bw = new BufferedWriter(fw);

            //Remove duplicate links
            ArrayList<String> unique = removeDuplicates(list);

            //Write each url with newline
            for (String element : unique) {
                bw.write(element);
                bw.write("\r\n");
            }

            //Closing BufferedWriter Stream
            bw.close();
        }
    }

    
    static ArrayList<String> removeDuplicates(ArrayList<String> list) {

        //Store unique items in result.
        ArrayList<String> result = new ArrayList<>();

        //Record encountered Strings in HashSet.
        HashSet<String> set = new HashSet<>();

        //Loop over argument list.
        for (String item : list) {

            //If String is not in set, add it to the list and the set.
            if (!set.contains(item)) {
                result.add(item);
                set.add(item);
            }
        }
        return result;
    }

}