import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ingredientListBBC{
    public static void main(String args[]) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        int totalCount = 0;
        int commonUse = 0;
        String alphabet = "abcdefghijklmnopqrstuvwxyz";

        
        for (int i = 0; i < alphabet.length(); i++) {
            //read the website
            Document doc = null;
            try {
                doc = Jsoup.connect("http://www.bbc.co.uk/food/ingredients/by/letter/" + alphabet.substring(0 + i, 1 + i)).get();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Elements imgs =  doc.select("img");

            for (Element alts : imgs) {
                String altText = alts.attr("alt");
                //Look through all image tags with an alt text and add to list.
                if (!altText.isEmpty() && !altText.equals("BBC") && !altText.equals("Search the BBC")) {
                    //System.out.println(alts.attr("alt"));
                    list.add(altText);
                }
            }
        }    

//         for (int j = 0; j < list.size(); j++) {
//             System.out.println(list.get(j));
//             totalCount++;
//         }

//        System.out.println("Total word count:" + totalCount);
        
        final long startTime = System.currentTimeMillis();
        for (String s : list) {
           if (s.length() > 2)
                Importer.create_new_ingredient(s,commonUse);
        }
        final long endTime = System.currentTimeMillis();
        System.out.println("Took "+(endTime - startTime)+" ms to import all ingredients. ("+(endTime-startTime)/1000+" s)");
    }
}