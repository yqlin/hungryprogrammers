import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;


public class Scrap 
{
	
	Scrap()
	{
		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton eatWell = new JButton("Eat Well");
		JButton foodNet = new JButton("Food Network");
		JButton allRecipes = new JButton("All Recipes");
		JButton allsites = new JButton("Scrape all Sites");
		new Importer().createTables();
		frame.getContentPane().add(allRecipes);
		frame.getContentPane().add(foodNet);
		frame.getContentPane().add(eatWell);
		frame.getContentPane().add(allsites);
		frame.setVisible(true);
		
		foodNet.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try {
					foodNetworkScraper.getInfo();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		allRecipes.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try {
					allRecipesScraper.getInfo();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		eatWell.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				try {
					eatingWellScraper.getInfo();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		allsites.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					eatingWellScraper.getInfo();
					allRecipesScraper.getInfo();
					foodNetworkScraper.getInfo();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		});
	}
	public static void main(String [] args)
	{
		new Scrap();
	}
}
