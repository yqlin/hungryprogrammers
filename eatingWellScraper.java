import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class eatingWellScraper {
    // Recipe info init
    protected String recipetitle = null;
    protected double recipeRating = 0;
    protected int numRating = 0;
    protected String imageURL = null;
    protected int value = 0;
    protected String recipeURL = null;
    
    // List of ingredients
    protected LinkedList<String> ingredients = null;
    
    // List of directions
    protected LinkedList<String> directions = null;
    
    protected eatingWellScraper(String URL) {
        Document doc = null;
        try {
            doc = Jsoup.connect(URL).get();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println();
            System.out.println("UNABLE TO CONNECT TO URL.");
        }
        
        //Url of the recipe itself (Reference to original recipe like Stehpanie said).
        recipeURL = URL;
        
        // Grabs title of recipe
        recipetitle = doc.title();
        recipetitle = recipetitle.replace(" Recipe | Eating Well", "");
        //System.out.println(recipetitle);
        
        // Grab rating of recipe
        Elements ratingText = doc.select("[itemprop = ratingvalue]");
        String content = ratingText.text();
        if(!content.isEmpty()){
			recipeRating = Double.parseDouble(content);
		}
		else
			recipeRating = 0;
        
        // Grab number of ratings
        Elements numRatingText = doc.select("[itemprop = ratingCount]");        
        String numRatingString = numRatingText.text();
        if(!numRatingString.isEmpty()){
			numRating = Integer.parseInt(numRatingString);
		}
		else
			numRating = 0;
        
        // Fills LinkedList with ingredients of recipe
        Elements ingredsText = doc.select("[itemprop = ingredients]");
        ingredients = new LinkedList<String>();
        for (Element ingredsElement : ingredsText.select("li"))
            ingredients.add(ingredsElement.text());
        
        
        
        //Elements ingredsText = doc.getElementsByClass("tabinfo");
        //ingredients = new LinkedList<String>();
        //for (Element ingredsElement : ingredsText.select("li"))
        //  ingredients.add(ingredsElement.text());
        
        // Fills LinkedLIst with directions of recipe
        Elements dir = doc.select("[itemprop = recipeinstructions]");
        directions = new LinkedList<String>();
        for (Element dirElement : dir.select("li"))
            directions.add(dirElement.text());
        
        // Grab image url
        Elements imageLink = doc.select("div#recipeImage");
        for (Element e : imageLink.select("img")) {
            imageURL = e.attr("src");
        }
    }
    
    public static void getInfo() throws IOException {
        ArrayList<String> urls = eatingWellList.getLinks();
        
        final long startTime = System.currentTimeMillis();
        for (String s : urls)
        {
            eatingWellScraper scraper = new eatingWellScraper(s);
            String name = scraper.recipetitle;
            double rating = scraper.recipeRating;
            int numRatings = scraper.numRating;
            //Category variable missing?
            String category = "";
            String image_url = scraper.imageURL;
            int value = scraper.value;
            String recipeURL = scraper.recipeURL;
            LinkedList<String> ingredients = scraper.ingredients;
            LinkedList<String> directions_list = scraper.directions;
            String directions = "";
            for (String z : directions_list)
                directions += z +" ";
            directions.trim();
            
            //Call the Importer create_recipe method
            int recipe_id = Importer.create_recipe(name,rating,numRatings,category,directions,0,image_url,value,recipeURL);
            
            //Iterate through ingredients and add each one to the Ingredients table, then add the link to the ingredients_in_recipes tabl   e...
            int count = 0;
            for (String z : ingredients) {
                int ingredient_id = Importer.create_ingredient(z);
                Importer.create_link(recipe_id, ingredient_id);
                
                String [] split = z.split(" ");
                for (String q : split)
                {
                    if (q.length() > 2)
                        Importer.create_new_link(recipe_id, q);
                }
                //System.out.println("Ingredient for: "+name+" - \'"+z+"\'\n"+ingredient_id+"\n");
                count++;
            }
            //System.out.println("total # of ingredients = "+count);
            
        }
        final long endTime = System.currentTimeMillis();
        System.out.println("Took "+(endTime - startTime)+" ms to import all recipes (EatingWell - "+urls.size()+" recipes). ("+(endTime-startTime)/1000+" s)");
    }
    
    public static void main(String args[]) throws IOException {
        getInfo();
    }
    
}

