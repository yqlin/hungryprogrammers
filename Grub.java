import javax.swing.*;

import java.net.URL;
import java.sql.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.border.*;
public class Grub extends JFrame
{
	static URL location [] = new URL[77];
	static String title [] = new String[77];
	static String ingredient [] = new String[77];
	Grub()
	{
		String DB_URL = "jdbc:mysql://localhost/hungry_programmers";
		String USER = "root";
		String PASS = "";
		Connection conn = null;
		Statement stmt = null;
		try
		{
		      Class.forName("com.mysql.jdbc.Driver");
		      conn = DriverManager.getConnection(DB_URL, USER, PASS);
		      stmt = conn.createStatement();

		      ResultSet rs = null;
		      String sql = "SELECT name, image_url FROM recipes WHERE image_url IS NOT NULL";
		      rs = stmt.executeQuery(sql);
		      int count = 0;
		      while (rs.next()) 
		      {
				String second = rs.getString("name");
				String first = rs.getString("image_url");
				location[count] = new URL(first);
				title[count] = new String(second);
				System.out.println(first);
				count++;
				System.out.println("Count is: " + count);
		      }
				//System.out.println("Count is: " + count);
			
			rs.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            conn.close();
		      }catch(SQLException se){
		      }// do nothing
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   System.out.println("Goodbye!");	
	}
}
