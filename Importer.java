import java.sql.*;

public class Importer {
    final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //URL to the DB. 
    final static String DB_URL = "jdbc:mysql://localhost";
    //Login credientials. Ensure that yours are here.
    final static String USER = "root";
    final static String PASS = "";

    static boolean createTables()
    {
        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute an insert
            //System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql = "CREATE SCHEMA if not exists `hungry_programmers`;";
            stmt.executeUpdate(sql);
            System.out.println("schema created");

            sql = "CREATE TABLE if not exists `hungry_programmers`.`recipes` ("
            +"`id` INT NOT NULL AUTO_INCREMENT,"
            +"`name` VARCHAR(250) NOT NULL,"
            +"`rating` DECIMAL NULL,"
            +"`num_ratings` INT NULL,"
            +"`category` VARCHAR(45) NULL,"
            +"`instructions` BLOB NULL,"
            +"`user_rating` DECIMAL NULL,"
            +"`image_url` VARCHAR(250) NULL,"
            +"`value` INT NULL,"
            +"`recipeURL` VARCHAR(250) NULL,"
            +"PRIMARY KEY (`id`),"
            +"UNIQUE INDEX `name_UNIQUE` (`name` ASC));";
            stmt.executeUpdate(sql);
            System.out.println("recipes table created.");

            sql = "CREATE TABLE if not exists `hungry_programmers`.`ingredients` ("
            +"`id` INT NOT NULL AUTO_INCREMENT,"
            +"`name` VARCHAR(250) NOT NULL,"
            +"`commonuse` INT NULL,"
            +"`newset` BOOLEAN NULL,"
            +"PRIMARY KEY (`id`),"
            +"UNIQUE INDEX `name_UNIQUE` (`name` ASC));";
            stmt.executeUpdate(sql);
            System.out.println("ingredients table created.");

            sql = "CREATE TABLE if not exists `hungry_programmers`.`ingredients_in_recipes` ("
            +"`recipe_id` INT NOT NULL,"
            +"`ingredient_id` INT NOT NULL,"
            +"PRIMARY KEY (`recipe_id`, `ingredient_id`),"
            +"INDEX `ingredient_fk_idx` (`ingredient_id` ASC),"
            +"CONSTRAINT `recipe_fk`"
            +"FOREIGN KEY (`recipe_id`)"
            +"REFERENCES `hungry_programmers`.`recipes` (`id`)"
            +"ON DELETE CASCADE "
            +" ON UPDATE CASCADE, "
            +"CONSTRAINT `ingredient_fk`"
            +"FOREIGN KEY (`ingredient_id`)"
            +"REFERENCES `hungry_programmers`.`ingredients` (`id`)"
            +"ON DELETE CASCADE "
            +"ON UPDATE CASCADE );";
            stmt.executeUpdate(sql);
            System.out.println("ingredients_link table created.");

            sql = "CREATE TABLE if not exists `hungry_programmers`.`new_ingredients` ("
            +"`id` INT NOT NULL AUTO_INCREMENT, "
            +"`name` VARCHAR(75) NULL, "
            +"`commonuse` INT NULL,"
            +"PRIMARY KEY (`id`), "
            +"UNIQUE INDEX `name_UNIQUE` (`name` ASC));";
            stmt.executeUpdate(sql);
            System.out.println("new ingredients table created.");

            sql = "CREATE TABLE  if not exists `hungry_programmers`.`new_ingredients_link` ("
            +"`recipe_id` INT NOT NULL, "
            +"`ingredient_id` INT NOT NULL, "
            +"PRIMARY KEY (`recipe_id`, `ingredient_id`), "
            +"INDEX `ingredient_fk_idx` (`ingredient_id` ASC), "
            +"CONSTRAINT `new_recipe_fk` "
            +"FOREIGN KEY (`recipe_id`) "
            +"REFERENCES `hungry_programmers`.`recipes` (`id`) "
            +"ON DELETE NO ACTION "
            +"ON UPDATE NO ACTION, "
            +"CONSTRAINT `new_ingredient_fk` "
            +"FOREIGN KEY (`ingredient_id`) "
            +"REFERENCES `hungry_programmers`.`new_ingredients` (`id`) "
            +"ON DELETE NO ACTION "
            +"ON UPDATE NO ACTION);";
            stmt.executeUpdate(sql);
            System.out.println("new_ingredients_link table created.");

            
            stmt.close();
            conn.close();
            //System.out.println("Update completed");
            return true;
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        return false;
    }

    //Takes in parameters for adding a Recipe Record, returns the ID of the recipe created
    public static int create_recipe(String name, double rating, int num_ratings, String category, String instructions, double user_rating, String image_url, int value, String recipeURL)
    {
        Connection conn = null;
        Statement stmt = null;
        PreparedStatement pstmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute an insert
            //System.out.println("Creating statement...");
            pstmt = conn.prepareStatement("INSERT IGNORE INTO `hungry_programmers`.`recipes` (`name`, `rating`, `num_ratings`, `category`, `instructions`, `user_rating`, `image_url`,`value`,`recipeURL`)"
                +" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            pstmt.setString(1, name);
            pstmt.setBigDecimal(2,  new java.math.BigDecimal(rating)); 
            pstmt.setInt(3, num_ratings);
            pstmt.setString(4, category);
            pstmt.setString(5, instructions);
            pstmt.setBigDecimal(6, new java.math.BigDecimal(0.0));
            pstmt.setString(7, image_url);
            pstmt.setInt(8, value);
            pstmt.setString(9, recipeURL);
            pstmt.executeUpdate();
            //String sql = "INSERT ignore INTO `hungry_programmers`.`recipes` (`name`,`rating`,`num_ratings`,`category`,`instructions`,`user_rating`,`image_url`) VALUES (\'"+name+"\',"+rating+","+num_ratings+",\'"+category+"\',\'"+instructions+"\'"
            //    +","+user_rating+",\'"+image_url+"\');";
            //pstmt.executeUpdate();

            pstmt = conn.prepareStatement("SELECT id from hungry_programmers.recipes where name = ?");
            pstmt.setString(1,name);
            ResultSet rs = pstmt.executeQuery();
            //STEP 5: Extract data from result set
            int id = 0;
            while(rs.next()){
                id = rs.getInt("id");
            }

            pstmt.close();
            conn.close();
            //System.out.println("Update completed");
            return id;
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Failed adding recipe: "+name);
        return -1;
    }

    //Takes in the parameters for adding an Ingredient Record, returns the ID of the ingredient added
    public static int create_ingredient(String name)
    {
        Connection conn = null;
        Statement stmt = null;
        PreparedStatement pstmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute an insert
            // System.out.println("Creating statement...");
            pstmt = conn.prepareStatement("INSERT IGNORE INTO  `hungry_programmers`.`ingredients` (`name`) VALUES (?);");
            pstmt.setString(1, name);
            pstmt.executeUpdate();
            //String sql = "INSERT ignore INTO `hungry_programmers`.`ingredients` (`name`) VALUES (\'"+name+"\');";
            //stmt.executeUpdate(sql);

            pstmt = conn.prepareStatement("SELECT id from hungry_programmers.ingredients where name = ?");
            pstmt.setString(1,name);
            ResultSet rs = pstmt.executeQuery();
            //STEP 5: Extract data from result set
            int id = 0;
            while(rs.next()){
                id = rs.getInt("id");
            }

            pstmt.close();
            conn.close();
            //System.out.println("Update completed");
            return id;
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
                if(pstmt!=null)
                    pstmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Failed adding ingredient: "+name);
        return -1;
    }
    
    public static boolean create_new_ingredient(String name, int commonUse)
    {
        Connection conn = null;
        Statement stmt = null;
        PreparedStatement pstmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute an insert
            pstmt = conn.prepareStatement("INSERT IGNORE INTO  `hungry_programmers`.`new_ingredients` (`name`,`commonUse`) VALUES (?,?);");
            pstmt.setString(1, name);
            pstmt.setInt(2, commonUse);
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
            return true;
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
                if(pstmt!=null)
                    pstmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Failed adding ingredient: "+name);
        return false;
    }
    
    public static boolean create_new_link(int recipe_id, String ingredient)
    {
        Connection conn = null;
        Statement stmt = null;
        PreparedStatement pstmt = null;
        try{
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL,USER,PASS);


            pstmt = conn.prepareStatement("INSERT ignore INTO `hungry_programmers`.`new_ingredients_link` (`recipe_id`,`ingredient_id`) VALUES  (?, (Select id from hungry_programmers.new_ingredients where name like ?));");
            pstmt.setInt(1,recipe_id);
            pstmt.setString(2,"%"+ingredient+"%");
            
            //Uncomment this if you want to look at the insert statements for debugging
            //System.out.println(pstmt);
            
            pstmt.executeUpdate();
            pstmt.close();
            conn.close();
            
            return true;
        }catch(SQLException se){
            //Handle errors for JDBC
            //se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                //se.printStackTrace();
            }//end finally try
        }
        return false;
        
    }

    //Takes in the IDs of the Recipe/Ingredient records to link, returns true if successful
    public static boolean create_link(int recipe_id, int ingredient_id)
    {
        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute an insert
            // System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql = "INSERT ignore INTO `hungry_programmers`.`ingredients_in_recipes` (`recipe_id`,`ingredient_id`) VALUES  ("+recipe_id+", "+ingredient_id+");";
            stmt.executeUpdate(sql);

            stmt.close();
            conn.close();
            //System.out.println("Update completed");
            return true;
        }catch(SQLException se){
            //Handle errors for JDBC
            System.out.println("Failed when adding: "+recipe_id+","+ingredient_id);
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        return false;
    }

    public static void main(String[]args)
    {
        createTables();
        //Run All Recipes
    }
}
