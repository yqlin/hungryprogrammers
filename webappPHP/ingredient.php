<!DOCTYPE html>
<html>
<head>
    <title>Hungry Programmers</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div id="navbarcolor">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">

                <a id="logo" class="navbar-brand" href="index.php" style="color: white;">Hungry Programmers</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="ingredient.php">Input Your Own Ingredients</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="viewAll.php">View All Recipes</a></li>
                </ul>

                <form class="navbar-form navbar-left" method="post" action="search.php" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" name="searchPhrase" placeholder="Search" style="width: 300px;">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="account.php">Account</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<div class="row" >
    <div class="" style="width: 50%; margin: 0 auto;">
        <form class="form-horizontal" role="form" method="post" action="customsearch.php">
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <h1 style="color: #FFFFFF;">Enter custom ingredients:</h1>
                    <input type="text" class="form-control" id="ingredient1" name="ingredient1" value="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="text" class="form-control" id="ingredient2" name="ingredient2" value="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input type="text" class="form-control" id="ingredient3" name="ingredient3" value="">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <input id="submit" name="submit" type="submit" value="Search For Recipes" class="btn btn-primary">
                    <a href="index.php" class="btn btn-danger">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>

</body>
</html>