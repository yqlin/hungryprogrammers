<!DOCTYPE html>
<html>
<head>
    <title>Hungry Programmers</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div id="navbarcolor">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">

                <a id="logo" class="navbar-brand" href="index.php" style="color: white;">Hungry Programmers</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="ingredient.php">Input Your Own Ingredients</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="viewAll.php">View All Recipes</a></li>
                </ul>

                <form class="navbar-form navbar-left" method="post" action="search.php" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" name="searchPhrase" placeholder="Search" style="width: 300px;">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="account.php">Account</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<div class="container text-center">
    <?php
    $recipeID = "";
    $page;
    if(!isset($_POST["page"])) {
       $page = 1;
    }else{
        $page = $recipeID = $_POST["page"];
    }
    echo '<h2 style="color: #FFFFFF;">View All Recipes: Page ' . $page . '</h2>';
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "hungry_programmers";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "Select * from recipes where id <= 22*'$page' AND id > ('$page'- 1) * 22 + 1";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row

        while($row = $result->fetch_assoc()) {
            $recipeName = $row["name"];
            $recipeID = $row["id"];
            $imageURL = $row["image_url"];
            $rating = $row["rating"];
            $numRating = $row["num_ratings"];


            echo '<div class="col-md-4">';
            echo '<div class="well">';
            echo '<form action="recipe.php" method="post">';
            echo '<input type="hidden" value='.$recipeID.' name="recipeID" />';
            echo '<input type="image" src='.$imageURL.' name="submit" style="width: 308px; height: 308px;"/>';
            echo '</form>';
            echo '<div style="white-space:nowrap; overflow:hidden;">' . $recipeName . '</div>';
            echo '<br>';
            echo "Rating:" . $rating . "/5";
            echo '<br>';
            echo "Number of Ratings:" . $numRating;
            echo '</div>';
            echo '</div>';
        }
    } else {
        echo "0 results";
    }
    echo '</div>';
    echo '</div>';
    $conn->close();

    ?>

</div>

<div class="container text-center">
    <form action="viewAll.php" method="post">
        <input type="hidden" id="counter" name="page" value="<?php echo $page;?>"/>
        <button type="submit" onclick="previous()">Previous</button>
        <button type="submit" onclick="next()">Next</button>
    </form>
</div>

</body>

<script>
    function next()
    {
        var count=parseInt($('#counter').val());
        $('#counter').val(count+1);
    }

    function previous()
    {
        var count=parseInt($('#counter').val());
        if (count > 1) {
            $('#counter').val(count - 1);
        }
    }
</script>
</html>