<!DOCTYPE html>
<html>
<head>
    <title>Hungry Programmers</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div id="navbarcolor">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">

                <a id="logo" class="navbar-brand" href="index.php" style="color: white;">Hungry Programmers</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="ingredient.php">Input Your Own Ingredients</a></li>
                </ul>

                <ul class="nav navbar-nav">
                    <li><a href="viewAll.php">View All Recipes</a></li>
                </ul>

                <form class="navbar-form navbar-left" method="post" action="search.php" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" name="searchPhrase" placeholder="Search" style="width: 300px;">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="account.php">Account</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

    <?php

    if (isset($_POST["recipeID"])){
        $recipeID = $_POST["recipeID"];
    } else {
        $recipeID = $_GET['id'];
    }

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "hungry_programmers";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM recipes WHERE id = '$recipeID' ";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $recipeName = $row["name"];
            $recipeID = $row["id"];
            $imageURL = $row["image_url"];
            $rating = $row["rating"];
            $numRating = $row["num_ratings"];
            $userRating = $row["user_rating"];
            $directions = $row["instructions"];
            $recipeURL = $row["recipeURL"];

            echo '<div class="panel panel-default center-block" style="width: 90%">';
            echo '<div class="panel-heading">&nbsp;</div>';
            echo '<div class="panel-body">';
            echo '<h2 style="text-align:center;">'. $recipeName .'</h2>';

            echo '<hr>';

            echo '<div class ="col-md-3 col-md-offset-2" id="recipeImage">';
            echo '<img src='.$imageURL.' height="300" width="300">';
            
            echo '</div>';

            echo '<div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0" id="ingredientList">';
            $sql = "SELECT ingredients.name FROM recipes JOIN ingredients_in_recipes ON recipes.id = recipe_id JOIN ingredients ON ingredients.id = ingredient_id WHERE recipe_id = '$recipeID'";
            $result = $conn->query($sql);

            echo "<h3>Ingredients: </h3>";

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    $ingredientName = $row["name"];

                    echo $ingredientName;
                    echo '<br>';

                }
            } else {
                echo "0 results";
            }
            echo '</div>';

            echo '<br>';
            echo '<div class="col-md-6 col-md-offset-3">';

            echo '<hr>';     

            echo '<div id="rating" style="width: 50%; margin: 0 auto;">';
            echo "<b>Rating:</b>" . $rating . "/5" . " <br> <b>Number of Ratings:</b>" . $numRating;
            echo '<br>';
            echo "<b>User Rating: </b>" .  $userRating;

            echo '<form id="formRate" action="rate.php" method="post">';
            echo '<select name="myselect" id="myselect">';
            echo '<option value="1">1</option>';
            echo '<option value="2">2</option>';
            echo '<option value="3">3</option>';
            echo '<option value="4">4</option>';
            echo '<option value="5">5</option>';
            echo '</select>';
            echo '<input type="hidden" value='.$recipeID.' name="recipeID" />';
            echo '<input type="submit" value="Rate">';
            echo '</form>';
            echo '<br>';

            echo '</div>';

            echo "<h3>Directions: </h3>" . $directions;

            echo '</div>';

            echo '</div class="panel panel-default center-block">';

            echo '<div class="panel-footer" align="right">Source: '. $recipeURL .'</div>';
            echo '</div>';

        }


    } else {
        echo "0 results";
    }



    $conn->close();

    ?>

<hr>

<div id="recommended">Recommended Recipes</div>
<br>
<div class="container text-center">

    <?php
    $recipeID = "";
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "hungry_programmers";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "Select recipes.id, recipes.name, recipes.rating, recipes.num_ratings, recipes.image_url,sum(new_ingredients.commonUse) from hungry_programmers.recipes join hungry_programmers.new_ingredients_link on hungry_programmers.recipes.id = hungry_programmers.new_ingredients_link.recipe_id join hungry_programmers.new_ingredients on hungry_programmers.new_ingredients.id = hungry_programmers.new_ingredients_link.ingredient_id group by recipe_id order by RAND(), sum(new_ingredients.commonUse) desc limit 6";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row

        while($row = $result->fetch_assoc()) {
            $recipeName = $row["name"];
            $recipeID = $row["id"];
            $imageURL = $row["image_url"];
            $rating = $row["rating"];
            $numRating = $row["num_ratings"];


            echo '<div class="col-md-4">';
            echo '<div class="well">';
            echo '<form action="recipe.php" method="post">';
            echo '<input type="hidden" value='.$recipeID.' name="recipeID" />';
            echo '<input type="image" src='.$imageURL.' name="submit"  style="width: 308px; height: 308px;"/>';
            echo '</form>';
            echo '<div style="white-space:nowrap; overflow:hidden;">' . $recipeName . '</div>';
            echo '<br>';
            echo "Rating:" . $rating . "/5";
            echo '<br>';
            echo "Number of Ratings:" . $numRating;
            echo '</div>';
            echo '</div>';
        }
    } else {
        echo "0 results";
    }

    $conn->close();

    ?>

</div>


</body>

</html>