<!DOCTYPE html>
<html>
<head>
	<title>Hungry Programmers</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
<div id="navbarcolor">
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">

			<a id="logo" class="navbar-brand" href="index.php" style="color: white;">Hungry Programmers</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="ingredient.php">Input Your Own Ingredients</a></li>
			</ul>

			<ul class="nav navbar-nav">
				<li><a href="viewAll.php">View All Recipes</a></li>
			</ul>

			<form class="navbar-form navbar-left" method="post" action="search.php" role="search">
				<div class="form-group">
					<input type="text" class="form-control" name="searchPhrase" placeholder="Search" style="width: 300px;">
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="account.php">Account</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
</div>

<div class="container text-center">
	<div id="refresh"> <button class="btn btn-default" onclick="location.href = 'index.php';">Show More Recipes</div>
	<br>

	<?php
	$recipeID = "";
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "hungry_programmers";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

	$sql = "SELECT * FROM recipes WHERE image_url IS NOT NULL AND image_url NOT LIKE '%rackcdn%' ORDER BY RAND() LIMIT 6";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row

		while($row = $result->fetch_assoc()) {
			$recipeName = $row["name"];
			$recipeID = $row["id"];
			$imageURL = $row["image_url"];
			$rating = $row["rating"];
			$numRating = $row["num_ratings"];


			echo '<div class="col-md-4">';
			echo '<div class="well">';
			echo '<form action="recipe.php" method="post">';
			echo '<input type="hidden" value='.$recipeID.' name="recipeID" />';
			echo '<input type="image" src='.$imageURL.' name="submit"  style="width: 308px; height: 308px;"/>';
			echo '</form>';
			echo '<div style="white-space:nowrap; overflow:hidden;">' . $recipeName . '</div>';
			echo '<br>';
			echo "Rating:" . $rating . "/5";
			echo '<br>';
			echo "Number of Ratings:" . $numRating;
			echo '</div>';
			echo '</div>';
		}
	} else {
		echo "0 results";
	}

	$conn->close();

	?>
</div>



<hr>
<div id="recommended">Recommended Recipes</div>
<br>
<div class="container text-center">

	<?php
	$recipeID = "";
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "hungry_programmers";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

	$sql = "Select recipes.id, recipes.name, recipes.rating, recipes.num_ratings, recipes.image_url,sum(new_ingredients.commonUse) from hungry_programmers.recipes join hungry_programmers.new_ingredients_link on hungry_programmers.recipes.id = hungry_programmers.new_ingredients_link.recipe_id join hungry_programmers.new_ingredients on hungry_programmers.new_ingredients.id = hungry_programmers.new_ingredients_link.ingredient_id group by recipe_id order by RAND(), sum(new_ingredients.commonUse) desc limit 6";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row

		while($row = $result->fetch_assoc()) {
			$recipeName = $row["name"];
			$recipeID = $row["id"];
			$imageURL = $row["image_url"];
			$rating = $row["rating"];
			$numRating = $row["num_ratings"];


			echo '<div class="col-md-4">';
			echo '<div class="well">';
			echo '<form action="recipe.php" method="post">';
			echo '<input type="hidden" value='.$recipeID.' name="recipeID" />';
			echo '<input type="image" src='.$imageURL.' name="submit"  style="width: 308px; height: 308px;"/>';
			echo '</form>';
			echo '<div style="white-space:nowrap; overflow:hidden;">' . $recipeName . '</div>';
			echo '<br>';
			echo "Rating:" . $rating . "/5";
			echo '<br>';
			echo "Number of Ratings:" . $numRating;
			echo '</div>';
			echo '</div>';
		}
	} else {
		echo "0 results";
	}

	$conn->close();

	?>

</div>

</body>
</html>