import java.sql.*;
import java.util.*;

public class SuggestionsAlgorithms
{
    final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //URL to the DB. 
    final static String DB_URL = "jdbc:mysql://localhost";
    //Login credientials. Ensure that yours are here.
    final static String USER = "root";
    final static String PASS = "";
    //maybe it works?


    //Return the most popular Recipes that the user hasn't rated yet
    public static String[] GeneralSuggestion()
    {
        Connection conn = null;
        Statement stmt = null;

        String[]namelist = new String[10];
        int listnum = 0;

        try
        {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            stmt = conn.createStatement();
            //rating > 4 for most popular, also to trim any lists. user_rating = 0 because we don't want to suggest something the user has already seen
            String sqlinput = "SELECT * FROM hungry_programmers.recipes WHERE rating > 4 AND user_rating = 0 ORDER BY rating DESC LIMIT 10;";
            stmt.executeQuery(sqlinput);
            //standard operating procedure more or less
            ResultSet rs = stmt.executeQuery(sqlinput);
            String names = "";
            while(rs.next()){
                names = rs.getString("name");
                //safety notice, if  the listnum could risk throwing a stack overflow
                if(listnum > 10)
                    break;
                namelist[listnum] = names;
                listnum++;
            }
            //clean it up, send it back
            stmt.close();
            conn.close();
            return namelist;
	 	}
        //in the event of emergency, press all the buttons
    	catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
    	}
        //we can't just well return nothing now can we
		return namelist;
	}



    //like GeneralSuggestion, but this one is for ones the user likes the most
	public static String[] PersonalBests()
    {
        Connection conn = null;
        Statement stmt = null;

        String[]namelist = new String[10];
        int listnum = 0;
        //looks like GeneralSuggestion, but
        try
        {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            stmt = conn.createStatement();
            //this time we care about user_rating, not rating. That is to say, what does the user like the most
            String sqlinput = "SELECT * FROM hungry_programmers.recipes WHERE  user_rating > 4 ORDER BY rating DESC LIMIT 10;";
            stmt.executeQuery(sqlinput);

            ResultSet rs = stmt.executeQuery(sqlinput);
            String names = "";
            while(rs.next()){
                //might shift the given number up from ten, to some larger number, but for now...
                names = rs.getString("name");
                if(listnum > 10)
                    break;
                namelist[listnum] = names;
                listnum++;
            }
            stmt.close();
            conn.close();
            return namelist;
        }
        catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
        return namelist;
    }


    //this one was trickier overall.
	public static String ManualSearch(String searched)
    {
        Connection conn = null;
        Statement stmt = null;
        //trickier because we're taking input for one, so we need PREPARED STATEMENTS
        PreparedStatement prepstat = null;
        String searchitem = "";
        try
        {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //the name of the game is make the prepared statement, set the prepared statement, then work the prepared statement.
            prepstat = conn.prepareStatement("SELECT * FROM hungry_programmers.recipes WHERE name = ?;");
            prepstat.setString(1, searched);
            prepstat.executeUpdate();
            //this is where code goes
            //SELECT * FROM hungry_programmers.recipes WHERE name = \'"+searched+"\'";

            ResultSet rs = prepstat.executeQuery();
            int id = 0;
            String nameitem = "";
            double ratingitem = 0.0;
            int numratingitem = 0;
            String categoryitem = "";
            String instructionsitem = "";
            double userratingitem = 0.0;
            String imageurlitem = "";
            //set everything up
            while(rs.next()){
                id = rs.getInt("id");
                nameitem = rs.getString("name");
                ratingitem = rs.getDouble("rating");
                numratingitem = rs.getInt("num_ratings");
                categoryitem = rs.getString("category");
                instructionsitem = rs.getString("instructions");
                userratingitem = rs.getDouble("user_rating");
                imageurlitem = rs.getString("image_url");
            }
            //pass it all in. I sorted User rating higher for clarification purposes.
            searchitem = String.valueOf(id)
             + "\n Recipe Name: " + nameitem 
             + "\n Rating: " + String.valueOf(ratingitem) 
             + "\n # of Ratings: " + String.valueOf(numratingitem)
             + "\n Your Rating: " + String.valueOf(userratingitem)
             + "\n Category: " + categoryitem
             + "\n Instructions: " + instructionsitem
             + "\n Image: " + imageurlitem;
            stmt.close();
            conn.close();
            //magic?
            return searchitem;
	 	}
    	catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
    	}
		return searchitem;
	}


    //this is just a dupe main, not meant to be used.
    public static void main(String[]args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Suggested Recipes:\n");
        String[]gensug = GeneralSuggestion();
        for(int i = 0; i < gensug.length; i++)
        {
            System.out.println(gensug[i]);
        }


        System.out.println("\n\n\nUser favorites:\n");
        String[]perbes = PersonalBests();
        for(int i = 0; i < perbes.length; i++)
        {
            System.out.println(perbes[i]);
        }

        System.out.println("\n\n\n");
        System.out.println("Looking for a recipe?\n and ManualSearch(String searched) for finding particular recipes.");
        String req = input.nextLine();
        System.out.println("\nSearching for " + req +"\n");
        System.out.println(ManualSearch(req));
    }
}