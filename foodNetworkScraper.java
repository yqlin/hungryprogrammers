import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class foodNetworkScraper {
	// Recipe info init
	protected String recipetitle = null;
	protected double recipeRating = 0;
	protected int numRating = 0;
	protected String imageURL = null;
	protected int value = 0;
	protected String recipeURL = null;
	
	// List of ingredients
	protected LinkedList<String> ingredients = null;
	
	// List of directions
	protected LinkedList<String> directions = null;
	
	protected foodNetworkScraper(String URL) {
		Document doc = null;
		try {
			doc = Jsoup.connect(URL).get();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println();
			System.out.println("UNABLE TO CONNECT TO URL.");
		}
		
		//Url of the recipe itself (Reference to original recipe like Stehpanie said).
        recipeURL = URL;
		
		// Grabs title of recipe
		recipetitle = doc.title();
		recipetitle = recipetitle.replace(" Recipe : Food Network Kitchen : Food Network", "");
		
		// Grab rating of recipe
		//Elements ratingText = doc.getElementsByClass("rating-stars-img");
		//Element ratingElement = ratingText.select("meta").first();
		//String content = ratingElement.attr("content");
		//recipeRating = Double.parseDouble(content);
		
		// Grab number of ratings
		//Element readyInText = doc.getElementById("pRatings");
		//String numRatingString = readyInText.text();
		//numRatingString = numRatingString.replace(" Ratings", "");
		//numRating = Integer.parseInt(numRatingString);
		
		// Fills LinkedList with ingredients of recipe
		Elements ingredsText = doc.getElementsByClass("ingredients");
		ingredients = new LinkedList<String>();
		for (Element ingredsElement : ingredsText.select("li"))
			ingredients.add(ingredsElement.text());
		
		//Fills LinkedLIst with directions of recipe
		Elements dir = doc.getElementsByClass("directions");
		directions = new LinkedList<String>();
		for (Element dirElement : dir.select("p"))
			directions.add(dirElement.text());
		
		// Grab image url
		Elements imageLink = doc.select("a[href*=#lightbox-recipe-image]");
		for (Element e : imageLink.select("img")) {
			imageURL = e.attr("src");
		}
	}
	
	public static void getInfo() throws IOException {
        ArrayList<String> urls = foodNetworkList.getLinks();
        
        final long startTime = System.currentTimeMillis();
        for (String s : urls)
        {
            foodNetworkScraper scraper = new foodNetworkScraper(s);
            String name = scraper.recipetitle;
            double rating = scraper.recipeRating;
            int numRatings = scraper.numRating;
            //Category variable missing?
            String category = "";
            String image_url = scraper.imageURL;
            int value = scraper.value;
            String recipeURL = scraper.recipeURL;
            LinkedList<String> ingredients = scraper.ingredients;
            LinkedList<String> directions_list = scraper.directions;
            String directions = "";
            for (String z : directions_list)
                directions += z +" ";
            directions.trim();
            
            //Call the Importer create_recipe method
            int recipe_id = Importer.create_recipe(name,rating,numRatings,category,directions,0,image_url,value,recipeURL);
            
            //Iterate through ingredients and add each one to the Ingredients table, then add the link to the ingredients_in_recipes table...
            for (String z : ingredients) {
                int ingredient_id = Importer.create_ingredient(z);
                Importer.create_link(recipe_id, ingredient_id);
                String [] split = z.split(" ");
                for (String q : split)
                {
                    if (q.length() > 2)
                        Importer.create_new_link(recipe_id, q);
                }
            }
            
        }
        final long endTime = System.currentTimeMillis();
        System.out.println("Took "+(endTime - startTime)+" ms to import all recipes (FoodNetwork - "+urls.size()+" recipes). ("+(endTime-startTime)/1000+" s)");
    }
	
	public static void main(String args[]) throws IOException {
		getInfo();
	}
	
}

