import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class allRecipesList{

    public static ArrayList<String> getLinks() throws IOException
    {

        ArrayList<String> list = new ArrayList<>();

        //breakfast/brunch
        //Reading HTML page from URL, loop for 109 pages at the moment
        //Set at 2 for testing

        for (int i = 1; i < 108; i++) {
            Document doc = null;
            try {
                //if(j == 1)
                doc = Jsoup.connect("http://allrecipes.com/recipes/breakfast-and-brunch/main.aspx?soid=showcase_2&Page=" + i + "&vm=g&evt19=1&p34=HR_GridView#recipes").get();
                //else{
                //  doc = Jsoup.connect("http://allrecipes.com/recipes/lunch/main.aspx?soid=showcase_2&Page=" + i + "&vm=g&evt19=1&p34=HR_GridView#recipes").get();   
                //  System.out.println("Made it to second url.");
                //}
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Filter hrefs to only end with Detail.aspx?...
            Elements links = doc.select("a[href$=Detail.aspx?evt19=1&referringHubId=78]");

            //Loop through elements and only add the href to array list
            for (Element link : links) {
                list.add(link.attr("abs:href"));
            }
        }

        //lunch
        for (int i = 1; i < 30; i++) {
            Document doc2 = null;
            try {
                doc2 = Jsoup.connect("http://allrecipes.com/recipes/lunch/main.aspx?soid=showcase_2&Page=" + i + "&vm=g&evt19=1&p34=HR_GridView#recipes").get();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Filter hrefs to only end with Detail.aspx?...
            Elements links = doc2.select("a[href$=Detail.aspx?evt19=1&referringHubId=17561]");

            //Loop through elements and only add the href to array list
            for (Element link : links) {
                list.add(link.attr("abs:href"));
            }
        }

        //dinner
        for (int i = 1; i < 48; i++) {
            Document doc3 = null;
            try {
                doc3 = Jsoup.connect("http://allrecipes.com/recipes/dinner/main.aspx?soid=showcase_2&Page=" + i + "&vm=g&evt19=1&p34=HR_GridView#recipes").get();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Filter hrefs to only end with Detail.aspx?evt19=1
            Elements links = doc3.select("a[href$=Detail.aspx?evt19=1&referringHubId=17562]");

            //Loop through elements and only add the href to array list
            for (Element link : links) {
                list.add(link.attr("abs:href"));
            }
        }
        return removeDuplicates(list);
    }

    public static void main(String args[]) throws IOException{
        ArrayList<String> list = new ArrayList<>();
        File file = new File("Allrecipe.txt");

        //breakfast/brunch
        //Reading HTML page from URL, loop for 109 pages at the moment
        //Set at 2 for testing

        for (int i = 1; i < 2; i++) {
            Document doc = null;
            try {
                //if(j == 1)
                doc = Jsoup.connect("http://allrecipes.com/recipes/breakfast-and-brunch/main.aspx?soid=showcase_2&Page=" + i + "&vm=g&evt19=1&p34=HR_GridView#recipes").get();
                //else{
                //  doc = Jsoup.connect("http://allrecipes.com/recipes/lunch/main.aspx?soid=showcase_2&Page=" + i + "&vm=g&evt19=1&p34=HR_GridView#recipes").get();   
                //  System.out.println("Made it to second url.");
                //}
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Filter hrefs to only end with Detail.aspx?...
            Elements links = doc.select("a[href$=Detail.aspx?evt19=1&referringHubId=78]");

            //Loop through elements and only add the href to array list
            for (Element link : links) {
                list.add(link.attr("abs:href"));
            }

            FileWriter fw = new FileWriter(file,false);
            BufferedWriter bw = new BufferedWriter(fw);

            //Remove duplicate links
            ArrayList<String> unique = removeDuplicates(list);

            //Write each url with newline
            for (String element : unique) {
              bw.write(element);
              bw.write("\r\n");
            }

            //Closing BufferedWriter Stream
            bw.close();
        }

        //  j++;
        //} while(j < 3);

        //lunch
        for (int i = 1; i < 2; i++) {
            Document doc2 = null;
            try {
                doc2 = Jsoup.connect("http://allrecipes.com/recipes/lunch/main.aspx?soid=showcase_2&Page=" + i + "&vm=g&evt19=1&p34=HR_GridView#recipes").get();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Filter hrefs to only end with Detail.aspx?...
            Elements links = doc2.select("a[href$=Detail.aspx?evt19=1&referringHubId=17561]");

            //Loop through elements and only add the href to array list
            for (Element link : links) {
                list.add(link.attr("abs:href"));
            }

             FileWriter fw = new FileWriter(file,false);
             BufferedWriter bw = new BufferedWriter(fw);

            //Remove duplicate links
            ArrayList<String> unique = removeDuplicates(list);

            //Write each url with newline
              for (String element : unique) {
              bw.write(element);
               bw.write("\r\n");
               }

            //Closing BufferedWriter Stream
               bw.close();
        }

        //dinner
        for (int i = 1; i < 2; i++) {
            Document doc3 = null;
            try {
                doc3 = Jsoup.connect("http://allrecipes.com/recipes/dinner/main.aspx?soid=showcase_2&Page=" + i + "&vm=g&evt19=1&p34=HR_GridView#recipes").get();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Filter hrefs to only end with Detail.aspx?evt19=1
            Elements links = doc3.select("a[href$=Detail.aspx?evt19=1&referringHubId=17562]");

            //Loop through elements and only add the href to array list
            for (Element link : links) {
                list.add(link.attr("abs:href"));
            }

             FileWriter fw = new FileWriter(file,false);
               BufferedWriter bw = new BufferedWriter(fw);

            //Remove duplicate links
            ArrayList<String> unique = removeDuplicates(list);

           //Write each url with newline
               for (String element : unique) {
               bw.write(element);
               bw.write("\r\n");
               }

            //Closing BufferedWriter Stream
               bw.close();
        }
    }

    static ArrayList<String> removeDuplicates(ArrayList<String> list) {

        //Store unique items in result.
        ArrayList<String> result = new ArrayList<>();

        //Record encountered Strings in HashSet.
        HashSet<String> set = new HashSet<>();

        //Loop over argument list.
        for (String item : list) {

            //If String is not in set, add it to the list and the set.
            if (!set.contains(item)) {
                result.add(item);
                set.add(item);
            }
        }
        return result;
    }

}